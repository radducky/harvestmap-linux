# HarvestMap for Linux


This tiny script creates symlink in your $HOME/Documents so the [HarvestMap](https://www.esoui.com/downloads/info57-HarvestMap) macOS script starts to work. 
Also runs the script.

## How to use

1. Download hmdata_loader.sh
2. Open the script and change next params: 
```
ESO_DATA_DIR=
CLIENTS=
```
3. Run the script when you need to update HarvestMap data:
```
sh hmdata_loader.sh
```
