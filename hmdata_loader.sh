#!/bin/sh
### This script creates a symbolic link to the ESO directory in your $HOME/Documents for the HarvestMap MacOS script to work properly and starts it.
# https://gitlab.com/radducky/harvestmap-linux
### HarvestMap addon by Shinni: 
# https://www.esoui.com/downloads/info57-HarvestMap

### CUSTOMIZATION
# ESO data directory path
ESO_DATA_DIR="${HOME}/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/Documents/Elder Scrolls Online"
# ESO client type: LIVE, PTS, or both
# You can set one client:  CLIENTS="live"
#            or set both:  CLIENTS="live pts"
CLIENTS="live"

### SCRIPT
color="\e[1;33m"
no_color="\e[0m"
home_doc_dir="${HOME}/Documents"
if [ ! -d "${home_doc_dir}" ]
then
    mkdir "${home_doc_dir}"
fi
home_eso_dir="${home_doc_dir}/Elder Scrolls Online"
if [ -L "${home_eso_dir}" ] || [ ! -d "${home_eso_dir}" ]
then
    rm "${home_eso_dir}"
    echo "${color}Create symlink for:${no_color}"
    echo $ESO_DATA_DIR
    ln -s "${ESO_DATA_DIR}" "${home_eso_dir}"
fi
for client in $CLIENTS
do
    if [ -d "${home_eso_dir}/${client}" ]
    then
        echo "${color}Manage HarvestMap Data (${client})${no_color}"
        bash "${home_eso_dir}/${client}/AddOns/HarvestMapData/DownloadNewData.command"
    fi
done
echo "${color}Done.${no_color}"
sleep 3
